import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { requestLogout } from 'redux/actions/Auth';
import Layout from 'components/layout/Main';
// import auth from 'utils/Auth';

class Logout extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const { logout } = this.props;

    // const loginData = auth.loggedInData();

    logout();
    localStorage.clear();
  }

  render() {
    return <Layout noHeader />;
  }
}

Logout.propTypes = {
  logout: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(requestLogout()),
});

export default connect(null, mapDispatchToProps)(Logout);
