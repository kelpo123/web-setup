import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import auth from 'helpers/Auth';

class NoProtected extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
      allow: false,
    };
  }

  componentDidMount() {
    const isLoggedIn = auth.isLoggedIn();

    this.setState(() => {
      return {
        initialized: true,
      };
    });

    if (isLoggedIn) {
      this.setState(() => {
        return {
          allow: true,
        };
      });
    }
  }

  render() {
    const { initialized, allow } = this.state;
    const { allow: allowProps, redirect, children } = this.props;

    if (!initialized) {
      return null;
    }

    if (allow || allowProps) {
      return <Redirect push={false} to={redirect} />;
    }

    return children;
  }
}

NoProtected.propTypes = {
  allow: PropTypes.bool,
  redirect: PropTypes.string.isRequired,
  children: PropTypes.object.isRequired,
};

export default NoProtected;
