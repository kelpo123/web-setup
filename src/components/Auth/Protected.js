import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

import auth from 'helpers/Auth';

class Protected extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
      allow: false,
    };
  }

  componentDidMount() {
    const isLoggedIn = auth.isLoggedIn();
    if (!isLoggedIn) {
      this.setState(() => {
        return {
          initialized: true,
          allow: false,
        };
      });
    } else {
      this.setState(() => {
        return {
          initialized: true,
          allow: true,
        };
      });
    }
  }

  render() {
    const { initialized, allow } = this.state;
    const { children } = this.props;
    if (!initialized) {
      return null;
    }

    if (allow) {
      return children;
    }

    return (
      <Route
        render={({ staticContext }) => {
          if (staticContext) staticContext.status = 403;
          return <Redirect push={false} to="/" />;
        }}
      />
    );
  }
}

Protected.propTypes = {
  children: PropTypes.object.isRequired,
};

export default Protected;
