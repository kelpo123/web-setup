import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Layout, Row, Col } from 'antd';

const { Footer } = Layout;

const FooterLayout = memo(function FooterLayout(props) {
  const { riseFooter } = props;
  return (
    <Footer className={riseFooter ? 'rise-footer' : ''}>
      <Row className="ant-footer-wrapper">
        <Col className="content-padding">
          <span>Powered By</span>
          <div className="footer-content">
            <Link to="/">
              {` ${process.env.REACT_APP_NAME}`}
              <span>.com</span>
            </Link>
          </div>
        </Col>
      </Row>
    </Footer>
  );
});

FooterLayout.propTypes = {
  riseFooter: PropTypes.bool,
};

export default FooterLayout;
