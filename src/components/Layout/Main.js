import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';

import Header from 'components/layout/Header';
import Footer from 'components/layout/Footer';

const { Content } = Layout;

const MainLayout = memo(function MainLayout(props) {
  const {
    children,
    className,
    noHeader,
    noFooter,
    noPadding,
    centerMiddle,
    fullWidth,
  } = props;

  const centerLayout = centerMiddle ? 'center-middle' : '';
  const noPaddLayout = noPadding ? ' no-padding' : '';
  const fullWidthLayout = fullWidth ? 'w-100' : '';
  return (
    <Layout className={`${className} ${fullWidthLayout}`}>
      {noHeader ? null : <Header {...props} />}
      <Content
        className={`${centerLayout}${noPaddLayout}${fullWidthLayout}`}
        style={noHeader ? { marginTop: 0 } : {}}
      >
        {children}
      </Content>
      {noFooter ? null : <Footer {...props} />}
    </Layout>
  );
});
MainLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.object.isRequired,
    PropTypes.array.isRequired,
  ]),
  className: PropTypes.string,
  noHeader: PropTypes.bool,
  noFooter: PropTypes.bool,
  noPadding: PropTypes.bool,
  centerMiddle: PropTypes.bool,
};

export default MainLayout;
