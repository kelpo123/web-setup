import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Layout, Row, Col, PageHeader } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
const { Header } = Layout;

const HeaderLayout = memo(function HeaderLayout(props) {
  const {
    handleClose,
    closeable,
    extraHeaderContent,
    headerTitle,
    noHeaderTitle,
    headerShadow,
    headerClassName,
    extraHeader,
  } = props;
  const noTitle = noHeaderTitle ? 'no-title' : '';
  const noBack = extraHeader ? '' : closeable ? ' no-back ' : ' no-close ';
  const title = headerTitle;
  const withShadow = headerShadow ? ' shadow' : '';
  return (
    <Header className={`${withShadow}`}>
      <Row>
        <Col className="content-padding">
          <PageHeader
            className={`${noTitle}${noBack}${headerClassName}`}
            title={title}
            center-title
            extra={
              extraHeader ? (
                extraHeaderContent
              ) : (
                <CloseOutlined onClick={() => handleClose()} />
              )
            }
            onBack={() => this.onBackHandle()}
          />
        </Col>
      </Row>
    </Header>
  );
});

HeaderLayout.propTypes = {
  handleClose: PropTypes.func,
  headerTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  extraHeaderContent: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  headerClassName: PropTypes.string,
  noHeaderTitle: PropTypes.bool,
  extraHeader: PropTypes.bool,
  closeable: PropTypes.bool,
  history: PropTypes.object.isRequired,
};

export default HeaderLayout;
