import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import routes from 'routes';

import { initializeStore } from './redux';
import './resources/style/style.less';
import './resources/style/util.scss';

const store = initializeStore();
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          {/* <div>a</div> */}
          {/* <Route path="/404" render={props => <Page404 {...props} />} /> */}
          {/* <Route path="/login" render={props => <Login {...props} />} /> */}
          {/* <Route path="/" render={props => <DefaultLayout {...props} />} /> */}
          {routes.map((route, idx) =>
            route.component ? (
              <Route
                key={idx}
                path={route.path}
                exact={route.exact}
                render={(propsComponent) => (
                  <route.component {...propsComponent} />
                )}
              />
            ) : null,
          )}
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
