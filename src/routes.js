import HomePages from 'pages/Home';
import AuthPages from 'pages/Auth';
const routes = [...HomePages, ...AuthPages];

export default routes;
