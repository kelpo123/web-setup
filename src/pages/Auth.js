import LoginContainer from 'containers/auth/Login';
export default [
  {
    path: '/login',
    exact: true,
    component: LoginContainer,
  },
];
