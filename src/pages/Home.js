import HomeContainer from 'containers/home/Home';
export default [
  {
    path: '/',
    exact: true,
    component: HomeContainer,
  },
];
