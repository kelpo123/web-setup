import localStorage from 'localStorage';

import { LANG_KEY } from 'constants/Data';
import { types } from 'constants/Setting';
import { encrypt } from 'helpers/Crypto';

// eslint-disable-next-line import/prefer-default-export
export function switchLanguage(locale) {
  localStorage.setItem(LANG_KEY, encrypt(locale));
  return (dispatch) => {
    dispatch({ type: types.SWITCH_LANGUAGE, locale });
  };
}
