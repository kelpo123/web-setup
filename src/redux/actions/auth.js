// import { message } from 'antd';
import { reset } from 'redux-form';
import { sprintf } from 'sprintf-js';

import { types, URL_LOGIN } from 'constants/Auth';
import api from 'helpers/Api';
import auth from 'helpers/Auth';
import secureStorage from 'helpers/SecureStorage';

function reloadSession(source) {
  let user = {};
  const userLoggedIn = secureStorage.getItem(process.env.REACT_APP_KEY);

  try {
    if (userLoggedIn.attributes) {
      user = userLoggedIn;
    }
  } catch (e) {
    console.log(e.message);
  }

  return {
    type: source === 'signup' ? types.SIGNUP_SUCCESS : types.AUTH_LOGIN,
    user,
  };
}

export function requestLogin(params, data) {
  return (dispatch) => {
    dispatch({ type: types.AUTH_LOADING });
    api
      .post(sprintf(URL_LOGIN, { ...params }), data)
      .then((response) => {
        const source = 'login';
        const setData = { ...response.data.data, source };
        secureStorage.setItem(process.env.REACT_APP_KEY, setData);
        if (data.name !== '') {
          dispatch(reset('nameForm'));
        }

        dispatch(reloadSession(source));
        dispatch(reset('loginForm'));
      })
      .catch((error) => {
        dispatch({
          type: types.AUTH_FAILED,
          code: error.code,
          message: error.message,
        });
      });
  };
}

export function requestLogout() {
  auth.logout();

  return (dispatch) => {
    dispatch({ type: types.AUTH_LOGOUT });
  };
}

export function loginErrorReset() {
  return (dispatch) => {
    dispatch({ type: types.AUTH_RESET });
  };
}
