import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import auth from 'redux/reducers/Auth';
import setting from 'redux/reducers/Setting';

export default combineReducers({
  auth,
  setting,
  form: reduxFormReducer,
});
