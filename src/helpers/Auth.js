import secureStorage from 'helpers/SecureStorage';

export const logoutHandler = () => {
  secureStorage.removeItem(process.env.REACT_APP_KEY);
  // secureStorage.clear();
};

const getLoggedInData = () => {
  try {
    return secureStorage.getItem(process.env.REACT_APP_KEY);
  } catch (e) {
    logoutHandler();
    return false;
  }
};

const getLoggedInDataWaiter = () => {
  try {
    return secureStorage.getItem(process.env.REACT_APP_KEY_WAITER);
  } catch (e) {
    logoutHandler();
    return false;
  }
};

export default {
  isLoggedIn: () => {
    return (
      getLoggedInData() !== null &&
      Object.keys(getLoggedInData()).length > 0 &&
      getLoggedInData().constructor === Object
    );
  },
  loggedInData: () => {
    return getLoggedInData();
  },
  loggedInDataWaiter: () => {
    return getLoggedInDataWaiter();
  },
  isAllowed: (user, mod, permissions) => {
    return (
      undefined !==
      user.permissions.find((role) => {
        // find return bool
        if (role.module === mod) {
          if (permissions) {
            return permissions.some((permission) => {
              return role.permissions.includes(permission);
            });
          }
          return true;
        }
        return false;
      })
    );
  },
  logout: () => {
    logoutHandler();
  },
  logoutWaiter: () => {
    secureStorage.removeItem(process.env.REACT_APP_KEY_WAITER);
  },
};
