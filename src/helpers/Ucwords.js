export default function ucwords(str) {
  return str.toLowerCase().replace(/\b[a-z]/g, (letter) => {
    return letter.toUpperCase();
  });
}

export function trunc(text, limit) {
  return text.length > limit ? `${text.substr(0, limit)}..` : text;
}

