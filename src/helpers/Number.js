/**
 * Number.prototype.format(d, n, x, s, c)
 *
 * @param integer d: number
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 *
 * 12345678.9.format(2, 3, '.', ',');  // "12.345.678,90"
 * 123456.789.format(4, 4, ' ', ':');  // "12 3456:7890"
 * 12345678.9.format(0, 3, '-');       // "12-345-679"
 */

export default function number(d, n, x, s, c) {
  const re = `\\d(?=(\\d{${x || 3}})+${n > 0 ? '\\D' : '$'})`;
  const num = d.toFixed(Math.max(0, ~~n)); // eslint-disable-line

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), `$&${s || ','}`);
}
