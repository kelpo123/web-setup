import secureStorage from 'helpers/SecureStorage';
import { THEME } from 'constants/Data';

export function hexToRGB(hex, alpha) {
  var r = parseInt(hex.slice(1, 3), 16),
    g = parseInt(hex.slice(3, 5), 16),
    b = parseInt(hex.slice(5, 7), 16);
  if (alpha) {
    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
  } else {
    return 'rgb(' + r + ', ' + g + ', ' + b + ')';
  }
}

export function setTheme() {
  const defaultColor = secureStorage.getItem(THEME);
  if (defaultColor !== null) {
    document.body.style.setProperty('--primary-color', defaultColor);
    document.body.style.setProperty('--input-color', defaultColor);
    document.body.style.setProperty(
      '--input-hover-border-color',
      hexToRGB(defaultColor, 0.7)
    );
    document.body.style.setProperty(
      '--box-shadow-color',
      hexToRGB(defaultColor, 0.2)
    );
    document.body.style.setProperty(
      '--cart-box-color',
      hexToRGB(defaultColor, 0.5)
    );
  }
}
