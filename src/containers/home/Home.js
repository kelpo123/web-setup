import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import flatten from 'flat';
import { ConfigProvider, Row, Col } from 'antd';

import Home from 'components/home/Home';
import Layout from 'components/layout/Main';
import AppLocale from 'resources/lngProvider';

function HomeContainer(props) {
  const { setting } = props;
  const currentAppLocale = AppLocale[setting.locale.locale];
  return (
    <>
      <ConfigProvider locale={currentAppLocale.antd}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={flatten(currentAppLocale.messages)}
        >
          <Layout
            {...props}
            className="home-content"
            centerMiddle
            noHeader
            noFooter
          >
            <div className="ant-col ant-col-xs-22 ant-col-sm-22">
              <Row type="flex" justify="center">
                <Col xs={24} sm={22} className="text-center">
                  <Home {...props} />
                </Col>
              </Row>
            </div>
          </Layout>
        </IntlProvider>
      </ConfigProvider>
    </>
  );
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  setting: state.setting,
});

// const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, null)(HomeContainer);
