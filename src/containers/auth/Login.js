import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import flatten from 'flat';
import { IntlProvider } from 'react-intl';
import { ConfigProvider, Row, Col } from 'antd';

import NoProtected from 'components/auth/NoProtected';
import LoginForm from 'components/auth/LoginForm';
import Layout from 'components/layout/Main';
import AppLocale from 'resources/lngProvider';
import { requestLogin } from 'redux/actions/Auth';

function LoginContainer(props) {
  const handleSubmit = (data) => {
    console.log(data);
  };
  const [allow] = useState(false);
  const { setting } = props;
  const currentAppLocale = AppLocale[setting.locale.locale];
  return (
    <ConfigProvider locale={currentAppLocale.antd}>
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={flatten(currentAppLocale.messages)}
      >
        <NoProtected allow={allow} redirect={'/'}>
          <Layout {...props} centerMiddle noHeader noFooter>
            <div className="login-content ant-col ant-col-xs-22 ant-col-sm-22">
              <Row type="flex" justify="center">
                <Col xs={24} sm={22} className="text-center">
                  <LoginForm
                    onSubmit={(data) => handleSubmit(data, true)}
                    {...props}
                  />
                </Col>
              </Row>
            </div>
          </Layout>
        </NoProtected>
      </IntlProvider>
    </ConfigProvider>
  );
}

LoginContainer.propTypes = {
  loginHandle: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  setting: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  setting: state.setting,
});

const mapDispatchToProps = (dispatch) => ({
  loginHandle: (params, data) => dispatch(requestLogin(params, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
